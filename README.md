eBookshelf 
=========

eBookshelf is a python script to take a eBook collection from calibre, and display it as a virtual bookshelf with books that open in your prefered reader on click.

![My virtual bookshelf](ebookshelf.png)

Installation
============

eBookshelf is distributed on [PyPI](https://pypi.org) as a universal wheel and is available on
Linux/macOS and Windows and supports Python 3 and PyPy.

``` {.sourceCode .bash}
$ pip install ebookshelf
```

License
=======

eBookshelf is distributed under the terms of both

-   [MIT License](https://choosealicense.com/licenses/mit)
-   [Apache License, Version 2.0](https://choosealicense.com/licenses/apache-2.0)

at your option.
